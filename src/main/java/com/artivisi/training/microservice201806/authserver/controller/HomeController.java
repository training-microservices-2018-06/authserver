package com.artivisi.training.microservice201806.authserver.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

    @PreAuthorize("hasAnyAuthority('VIEW_TRANSAKSI', 'EDIT_TRANSAKSI')")
    @GetMapping("/home")
    public void home(Authentication currentUser) {
        LOGGER.info("Current user : "+currentUser);
    }
}
