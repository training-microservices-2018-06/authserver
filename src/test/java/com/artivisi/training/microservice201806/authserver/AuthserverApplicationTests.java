package com.artivisi.training.microservice201806.authserver;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthserverApplicationTests {

	@Autowired private PasswordEncoder passwordEncoder;

	@Test
	public void generatePassword() {
		String password = "test1234";
		String hashedPassword = passwordEncoder.encode(password);
		System.out.println("Hashed : "+hashedPassword);

		Assert.assertTrue(passwordEncoder.matches(password, hashedPassword));
		Assert.assertTrue(passwordEncoder
				.matches("testmanager",
						"$2a$10$RPB/8RrHOPBbUj0iYRy7hu7K2fMKEFIR5Cqb2oGyeKcRFY/sH0.Mi"));
	}

}

